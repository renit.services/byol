#![feature(test)]

extern crate atomic_counter;

#[macro_use]
extern crate lazy_static;

extern crate test;

pub mod parser;
pub mod lexer;
pub mod interpreter;
