use parser;
use std::collections::VecDeque;

use parser::{EvaluationError, IntegerExpression, LEnv, LVal, Operator, QExpression, SExpression};

mod builtins {
  use super::*;

  fn div_op(a: i64, b: i64) -> Result<i64, EvaluationError> {
    if b == 0 {
      return Err(EvaluationError::DivideByZero);
    }
    Ok(a / b)
  }

  pub fn head(_: &mut LEnv, lval: LVal) -> Result<parser::LVal, EvaluationError> {
    let mut arguments = lval.sub_expressions("inside head")?;
    if arguments.len() > 1 {
      return Err(parser::unexpected_expression(
        "head takes one qexpr argument",
        "inside head",
        arguments.pop_back().unwrap(),
      ));
    }
    if arguments.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        "head takes one qexpr argument",
      ));
    }

    let mut qexpr = arguments.pop_front().unwrap().qexpr("inside head")?;
    if qexpr.expressions.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        "empty qexpr passed to head",
      ));
    }

    let mut head = VecDeque::with_capacity(1);
    head.push_front(qexpr.expressions.pop_front().unwrap());

    Ok(LVal::QExpression(QExpression { expressions: head }))
  }

  pub fn tail(_: &mut LEnv, lval: LVal) -> Result<parser::LVal, EvaluationError> {
    let mut expressions = lval.sub_expressions("inside tail")?;
    if expressions.len() > 1 {
      return Err(parser::unexpected_expression(
        "tail takes one qexpr argument",
        "inside tail",
        expressions.pop_back().unwrap(),
      ));
    }
    if expressions.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        "tail takes one qexpr argument",
      ));
    }

    let mut qexpr = expressions.pop_front().unwrap().qexpr("inside tail")?;
    if qexpr.expressions.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        "empty qexpr passed to tail",
      ));
    }
    qexpr.expressions.pop_front();

    Ok(LVal::QExpression(QExpression {
      expressions: qexpr.expressions,
    }))
  }

  pub fn list(_: &mut LEnv, lval: LVal) -> Result<parser::LVal, EvaluationError> {
    let sexpr = lval.sexpr("inside list")?;

    let qexpr = Ok(LVal::QExpression(QExpression {
      expressions: sexpr.expressions,
    }));
    qexpr
  }

  fn operator(
    op: parser::Operator,
    sub_expressions: VecDeque<parser::LVal>,
  ) -> Result<parser::LVal, EvaluationError> {
    use parser::LVal::*;
    let mut expressions = sub_expressions;

    if expressions.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        op.name(),
      ));
    }

    let mut acc = expressions.pop_front().unwrap().integer(op.name())?.val;

    use parser::Operator::*;
    match op {
      Subtract if expressions.len() == 0 => acc *= -1,
      _ => {}
    }

    for sub_expr in expressions.into_iter() {
      let next = sub_expr.integer("inside operator")?.val;
      acc = match op {
        Add => acc + next,
        Subtract => acc - next,
        Multiply => acc * next,
        Divide => div_op(acc, next)?,
      };
    }
    Ok(Integer(IntegerExpression { val: acc }))
  }

  pub fn add(_: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    operator(Operator::Add, lval.sub_expressions("add")?)
  }

  pub fn subtract(_: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    operator(Operator::Subtract, lval.sub_expressions("subtract")?)
  }

  pub fn multiply(_: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    operator(Operator::Multiply, lval.sub_expressions("multiply")?)
  }

  pub fn divide(_: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    operator(Operator::Divide, lval.sub_expressions("divide")?)
  }

  pub fn eval(lenv: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    let mut arguments = lval.sub_expressions("eval")?;
    if arguments.len() > 1 {
      return Err(parser::unexpected_expression(
        "eval takes one qexpr argument",
        "eval",
        arguments.pop_back().unwrap(),
      ));
    }
    if arguments.len() == 0 {
      return Err(EvaluationError::WantMoreExpressions(
        "eval takes one qexpr argument",
      ));
    }
    let qexpr = arguments.pop_front().unwrap().qexpr("eval")?;
    let sexpr = SExpression {
      expressions: qexpr.expressions,
    };
    eval_sexpr(lenv, sexpr)
  }

  pub fn join(_: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    let sexpr = lval.sexpr("join")?;

    let mut expressions = VecDeque::new();
    for expr in sexpr.expressions.into_iter() {
      expressions.append(&mut expr.qexpr("join")?.expressions);
    }
    Ok(LVal::QExpression(QExpression { expressions }))
  }

  pub fn define(lenv: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError> {
    let mut arguments = lval.sub_expressions("define")?;

    let qexpr = arguments.pop_front().ok_or_else(
      || EvaluationError::WantMoreExpressions("expected a qexpr as the first argument to define")
    )?.qexpr("define")?;

    if arguments.len() != qexpr.expressions.len() {
      return Err(parser::unexpected_expression("incorrect number of symbols", "define",LVal::QExpression(qexpr)));
    }

    let mut symbol_ids = Vec::with_capacity(qexpr.expressions.len());
    for maybe_sym in qexpr.expressions {
      symbol_ids.push(maybe_sym.symbol("define")?.id)
    }

    for i in 0..symbol_ids.len() {
      lenv.env.insert(symbol_ids[i], arguments.pop_front().ok_or_else(||
        EvaluationError::WantMoreExpressions("expected more arguments in define and anothe error to have appeared")
      )?);
    }

    Ok(LVal::SExpression(SExpression{expressions: VecDeque::new()}))
  }
}

pub fn add_builtins(lenv: &mut LEnv) {
  lenv.define_builtin("list", builtins::list);
  lenv.define_builtin("head", builtins::head);
  lenv.define_builtin("tail", builtins::tail);
  lenv.define_builtin("join", builtins::join);
  lenv.define_builtin("eval", builtins::eval);
  lenv.define_builtin("def", builtins::define);

  lenv.define_builtin("+", builtins::add);
  lenv.define_builtin("-", builtins::subtract);
  lenv.define_builtin("*", builtins::multiply);
  lenv.define_builtin("/", builtins::divide);
}

pub fn eval(lenv: &mut LEnv, program: parser::Program) -> Result<parser::LVal, EvaluationError> {
  eval_expr(lenv, program.root)
}

fn eval_usr_fn(func: parser::UserFn, arguments: VecDeque<LVal>) -> Result<LVal, EvaluationError> {
  // set up env
  // eval sexpr
}

fn eval_expr(lenv: &mut LEnv, expr: LVal) -> Result<parser::LVal, EvaluationError> {
  use parser::LVal::*;
  let ret = match expr {
    Symbol(s) => lenv.get(&s),
    SExpression(sexpr) => eval_sexpr(lenv, sexpr),
    expr => Ok(expr.clone()),
  };
  ret
}

fn eval_sexpr(
  lenv: &mut LEnv,
  sexpr: parser::SExpression,
) -> Result<parser::LVal, EvaluationError> {
  if sexpr.expressions.len() == 0 {
    return Ok(parser::LVal::SExpression(sexpr.clone()));
  }

  let mut expressions = sexpr.expressions;

  if expressions.len() == 1 {
    return eval_expr(lenv, expressions.pop_front().unwrap());
  }

  for i in 0..expressions.len() {
    expressions[i] = eval_expr(lenv, expressions[i].clone())?;
  }

  let func = match expressions.pop_front() {
    Some(lval) => lval.function("in eval_sexpr")?,
    None => return Err(EvaluationError::WantMoreExpressions("expected a function")),
  };

  func(lenv, LVal::SExpression(SExpression { expressions }))
}

#[cfg(test)]
mod tests {
  use super::*;
  use test::Bencher;

  macro_rules! assert_eval_result {
    // The `tt` (token tree) designator is used for
    // operators and tokens.
    ($lenv:ident, $expected:literal, $expression:literal) => {
      assert_eq!($expected.to_owned(), format!("{}", eval_test_helper(&mut $lenv, $expression).unwrap()))
    };
}


  fn lenv_default() -> LEnv<'static> {
    LEnv::with_builtins(add_builtins)
  }

  fn eval_test_helper(lenv: &mut LEnv, code: &str) -> Result<parser::LVal, EvaluationError> {
    eval(lenv, parser::parse(code).unwrap())
  }

  #[test]
  fn test_format() {
    assert_eq!(
      "(+ 2 (* 7 6) (* 2 5))".to_owned(),
      format!("{}", parser::parse("+ 2 (* 7 6) (* 2 5)").unwrap())
    );
    assert_eq!(
      "{+ 2 (* 7 6) (* 2 5)}".to_owned(),
      format!("{}", parser::parse("{+ 2 (* 7 6) (* 2 5)}").unwrap())
    );
  }

  #[cfg(test)]
  fn wrap_integer_expr(i: i64) -> parser::LVal {
    parser::LVal::Integer(parser::IntegerExpression { val: i })
  }

  #[test]
  fn test_eval() {
    let mut lenv = lenv_default();
    assert_eq!(Ok(wrap_integer_expr(45)), eval_test_helper(&mut lenv, "* 9 5"));
    assert_eq!(Ok(wrap_integer_expr(-100)), eval_test_helper(&mut lenv, "(- 100)"));
  }

  #[test]
  fn test_builtin_eval() {
    let mut lenv = lenv_default();
    assert_eval_result!(lenv, "{1}", "eval {head (list 1 2 3 4)}");
    assert_eval_result!(lenv, "30", "(eval (head {+ - + - * /})) 10 20");
  }

  #[test]
  fn test_eval_qexpr() {
    let mut lenv = lenv_default();
    assert_eq!(
      "{+ 2 (* 7 6) (* 2 5)}".to_owned(),
      format!("{}", eval_test_helper(&mut lenv, "{+ 2 (* 7 6) (* 2 5)}").unwrap())
    );
    assert_eq!(
      "{1 2 3 4}",
      &format!("{}", eval_test_helper(&mut lenv, "list 1 2 3 4").unwrap())
    );
    assert_eq!(
      "{1}",
      &format!(
        "{}",
        eval_test_helper(&mut lenv, "eval {head (list 1 2 3 4)}").unwrap()
      )
    );
    assert_eq!(
      "{tail tail}",
      &format!("{}", eval_test_helper(&mut lenv, "tail {tail tail tail}").unwrap())
    );
    assert_eq!(
      "{6 7}",
      &format!(
        "{}",
        eval_test_helper(&mut lenv, "eval (tail {tail tail {5 6 7}})").unwrap()
      )
    );
    assert_eq!(
      "3",
      &format!(
        "{}",
        eval_test_helper(&mut lenv, "eval (head {(+ 1 2) (+ 10 20)})").unwrap()
      )
    );
  }

  #[test]
  fn test_div_zero() {
    let mut lenv = lenv_default();
    assert_eq!(
      Err(EvaluationError::DivideByZero),
      eval_test_helper(&mut lenv, "/ 10 0")
    )
  }

  #[test]
  fn test_parens() {
    let mut lenv = lenv_default();
    assert_eq!(Ok(wrap_integer_expr(23)), eval_test_helper(&mut lenv, "+ 3 (* 4 5)"))
  }

  #[bench]
  fn bench_add_two(b: &mut Bencher) {
    b.iter(|| 2 + 2);
  }

  #[bench]
  fn bench_lispy_add_two_(b: &mut Bencher) {
    let mut lenv = lenv_default();
    let program = parser::parse("+ 2 2").unwrap();

    add_builtins(&mut lenv);

    b.iter(|| eval(&mut lenv, program.clone()));
  }

  #[test]
  fn test_define() {
    let mut lenv = lenv_default();
    assert_eval_result!(lenv, "()", "def {x} 100");
    assert_eval_result!(lenv, "()", "def {y} 200");
    assert_eval_result!(lenv, "100", "x");
    assert_eval_result!(lenv, "200", "y");
    assert_eval_result!(lenv, "300", "+ x y");
    assert_eval_result!(lenv, "()", "def {a b} 5 6");
    assert_eval_result!(lenv, "11", "+ a b");
    assert_eval_result!(lenv, "()", "def {arglist} {a b x y}");
    assert_eval_result!(lenv, "{a b x y}", "arglist");
    assert_eval_result!(lenv, "()", "def arglist 1 2 3 4");
    assert_eval_result!(lenv, "{1 2 3 4}", "list a b x y");
  }
}
