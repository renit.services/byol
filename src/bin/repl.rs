#![feature(test)]

extern crate rustyline;
extern crate test;
extern crate byol;

use rustyline::error::ReadlineError;
use rustyline::Editor;

use byol::parser;
use byol::interpreter;

fn main() {
    // `()` can be used when no completer is required
    let mut rl = Editor::<()>::new();
    if rl.load_history("history.txt").is_err() {
        println!("No previous history.");
    }
    let mut lenv = parser::LEnv::with_builtins(interpreter::add_builtins);
    loop {
        let readline = rl.readline("lispy> ");
        let line = match readline {
            Ok(l) => {
                rl.add_history_entry(l.clone());
                l
            }
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break;
            }
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break;
            },
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        };
        let ast = match parser::parse(&line) {
            Err(x) => {
                println!("error parsing: {:?}", x);
                continue;
            }
            Ok(ast) => ast,
        };
        match interpreter::eval(&mut lenv, ast) {
            Ok(res) => println!("{}", res),
            Err(cause) => println!("Err: {}", cause),
        }
    }
    rl.save_history("history.txt").unwrap();
}
