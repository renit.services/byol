extern crate byol;

use std::time::Instant;

use byol::{parser, interpreter};


fn main() {
    // `()` can be used when no completer is required
    let mut lenv = parser::LEnv::with_builtins(interpreter::add_builtins);

    let ast = match parser::parse("+ 2 2") {
        Err(x) => {
            println!("error parsing: {:?}", x);
            panic!();
        }
        Ok(ast) => ast,
    };

    let mut results = Vec::<i64>::with_capacity(1_000_000);

    let t = Instant::now();
    for _ in 0..1_000_000 {
      results.push(interpreter::eval(&mut lenv, ast.clone()).unwrap().integer("bench main").unwrap().val);
    }

    println!("{:?} took {:?}", results.len(), t.elapsed());

    let mut results = Vec::<i64>::with_capacity(1_000_000);

    let t = Instant::now();
    for i in 0..1_000_000 {
      results.push(i + 3);
    }

    println!("{:?} took {:?}", results.len(), t.elapsed());
}
