use std::string::String;
use std::vec::Vec;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Location {
  pub offset: usize,
  pub line: u64,
  pub col: u64,
}

impl Location {
  fn empty() -> Location {
    Location{
      offset: 0,
      line: 0,
      col: 0,
    }
  }
}

#[derive(PartialEq, Debug, Clone)]
pub enum Token {
  LeftParen,
  RightParen,
  LeftBrace,
  RightBrace,
  // StringLiteral(String),
  IntegerLiteral(i64),
  SymbolToken(String),
  EOF,
}

#[derive(Debug, PartialEq, Clone)]
pub struct TokenValue {
  pub start: Location,
  pub end: Location,
  pub value: Token,
  pub text: String,
}

impl TokenValue {
  pub fn empty() -> TokenValue {
    TokenValue{
      start: Location::empty(),
      end: Location::empty(),
      value: Token::EOF,
      text: String::new(),
    }
  }
}

#[derive(Debug)]
pub struct TokenizeErrorDetails {
  msg: String,
  filename: String,
  location: Location,
}

#[derive(Debug)]
pub enum TokenizeError {
  IO(std::io::Error),
  ErrorTokenizing(TokenizeErrorDetails),
}

fn error_tokenizing(msg: &str,
  filename: &str,
  location: Location) -> TokenizeError {
  TokenizeError::ErrorTokenizing(TokenizeErrorDetails {
    msg: msg.to_owned(),
    filename: filename.to_owned(),
    location: location,
  })
}

struct Scanner {
  tokens: Vec<TokenValue>,
  contents: Vec<char>,
  filename: String,
  offset: usize,
  line: u64,
  col: u64,
  newline: bool,
  // Start of the current token
  start: Location,
  // Current token contents
  current_token: String,
  has_error: bool,
  errors: Vec<TokenizeErrorDetails>,
}

impl Scanner {
  pub fn new(contents: &str, filename: &str) -> Scanner {
    Scanner {
      tokens: Vec::new(),
      // We need this copy to deal w/ unicode
      contents: contents.chars().collect(),
      col: 0,
      line: 0,
      offset: 0,
      filename: filename.to_owned(),
      newline: false,
      start: Location {
        col: 0,
        line: 0,
        offset: 0,
      },
      current_token: String::new(),
      has_error: false,
      errors: Vec::new(),
    }
  }

  pub fn loc(&self) -> Location {
    Location {
      offset: self.offset,
      line: self.line,
      col: self.col,
    }
  }

  pub fn advance(&mut self) -> Option<char> {
    if self.offset >= self.contents.len() {
      return None
    }
    let next = self.contents[self.offset];
    self.offset += 1;
    if self.newline {
      self.line += 1;
      self.col = 0;
      self.newline = false;
    }
    self.col += 1;
    match next {
      '\n' => {
          self.newline = true;
      }
      _ => {}
    }
    self.current_token.push(next);
    Some(next)
  }

  pub fn is_at_end(&self) -> bool {
    self.offset >= self.contents.len()
  }

  pub fn peek(&mut self) -> Option<char> {
    if self.is_at_end() {
      return None;
    }
    Some(self.contents[self.offset])
  }

  pub fn previous(&mut self) -> Option<char> {
    if self.offset == 0 {
      None
    } else {
      Some(self.contents[self.offset - 1])
    }
  }

  pub fn push_token(&mut self, t: Token) {
    self.tokens.push(TokenValue {
      start: self.start,
      end: self.loc(),
      value: t,
      text: self.current_token.to_owned(),
    });
    self.start = self.loc();
    self.current_token.clear()
  }

  pub fn reset_token(&mut self) {
    self.start = self.loc();
    self.current_token.clear();
  }

  pub fn scan_number(&mut self) -> Option<Token> {
    let negative_factor = match self.previous() {
      Some('-') => -1,
      _ => 1,
    };
    loop {
      match self.peek() {
        Some(c) if c.is_ascii_digit() => {
          self.advance();
          continue;
        },
        _ => break,
      }
    }
    match self.current_token.parse::<i64>() {
      Ok(n) => Some(Token::IntegerLiteral(n * negative_factor)),
      Err(e) => {
        self.add_error(&format!("{}", e));
        None
      }
    }
  }

  #[inline]
  fn is_symbol_char(c: char) -> bool {
    match c {
      '+' | '-' | '/' | '*' => true,
      '_' => true,
      '\\' | '>' | '<' | '!' | '&' => true,
      _ => c.is_alphanumeric(),
    }
  }

  pub fn scan_symbol(&mut self) -> Option<Token> {
    loop {
      match self.peek() {
        Some(c) if Scanner::is_symbol_char(c) => {
          self.advance();
        },
        _ => break,
      }
    }
    Some(Token::SymbolToken(self.current_token.clone()))
  }

  pub fn add_error(&mut self, msg: &str) {
    self.has_error = true;
    self.errors.push(TokenizeErrorDetails {
      msg: msg.to_owned(),
      filename: self.filename.to_owned(),
      location: self.loc(),
    });
  }
}

pub fn scan(
  source: &str,
  file: &str,
) -> Result<Vec<TokenValue>, TokenizeError> {
  let mut scanner = Scanner::new(source, file);

  loop {
    let c = match scanner.advance() {
      None => break,
      Some(c) => c
    };

    use lexer::Token::*;
    let token = match c {
      '0' ..= '9' => scanner.scan_number(),
      '-' => {
        match scanner.peek() {
          Some(c) if c.is_ascii_digit() => scanner.scan_number(),
          _ => scanner.scan_symbol(),
        }
      },
      '(' => Some(LeftParen),
      ')' => Some(RightParen),
      '{' => Some(LeftBrace),
      '}' => Some(RightBrace),
      _ if c.is_whitespace() => None,
      _ if Scanner::is_symbol_char(c) => scanner.scan_symbol(),
      _ => {
        eprintln!("unexpected character '{}' at {:?}", c, scanner.loc());
        scanner.has_error = true;
        None
      }
    };
    match token {
      Some(t) => scanner.push_token(t),
      None => scanner.reset_token(),
    }
  }

  if scanner.has_error {
    return Err(error_tokenizing("unable to tokenize, check logs", file, scanner.loc()))
  }
  scanner.push_token(Token::EOF);
  return Ok(scanner.tokens);
}

#[test]
fn test_scanner() {
  let input = "+ 376 4 5 (+ 2 3) / +****";
  let scanned = scan(&input, "filename.txt");
  assert!(
    scanned.is_ok(),
    "scanned is ok"
  );
  for token in scanned.unwrap() {
    println!("{:?}", token);
  }
}

