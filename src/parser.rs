use lexer;

use std::collections::{HashMap, VecDeque};
use std::convert::From;
use std::fmt;

use atomic_counter::{AtomicCounter, RelaxedCounter};
use std::sync::{Arc, RwLock};

lazy_static! {
  static ref SYMBOL_TO_ID: Arc<RwLock<HashMap<String, usize>>> =
    Arc::new(RwLock::new(HashMap::new()));
  static ref ID_TO_SYMBOL: Arc<RwLock<HashMap<usize, String>>> =
    Arc::new(RwLock::new(HashMap::new()));
  static ref NEXT_ID: RelaxedCounter = RelaxedCounter::new(0);
}

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum Operator {
  Add,
  Multiply,
  Subtract,
  Divide,
}

impl Operator {
  pub fn name(&self) -> &'static str {
    use self::Operator::*;
    match self {
      Add => "+",
      Multiply => "*",
      Subtract => "-",
      Divide => "/",
    }
  }
}

impl fmt::Display for Operator {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}", self.name())
  }
}

#[derive(PartialEq, Clone)]
pub struct Symbol {
  pub id: usize,
}

impl fmt::Display for Symbol {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let locked = ID_TO_SYMBOL.read().unwrap();
    let name = match locked.get(&self.id) {
      Some(s) => s.clone(),
      None => format!("symbol {} not found", self.id),
    };
    write!(f, "{}", name)
  }
}

impl fmt::Debug for Symbol {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let locked = ID_TO_SYMBOL.read().unwrap();
    let name = match locked.get(&self.id) {
      Some(s) => s.clone(),
      None => format!("symbol {} not found", self.id),
    };
    write!(f, "SYMBOL:{}", name)
  }
}

fn write_expressions(f: &mut fmt::Formatter<'_>, expressions: &VecDeque<LVal>) -> fmt::Result {
  let mut first = true;
  for sub_expression in expressions {
    if first {
      first = false;
      write!(f, "{}", sub_expression)?;
    } else {
      write!(f, " {}", sub_expression)?;
    }
  }
  write!(f, "")
}

#[derive(Debug, PartialEq, Clone)]
pub struct SExpression {
  pub expressions: VecDeque<LVal>,
}

impl fmt::Display for SExpression {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "(")?;
    write_expressions(f, &self.expressions)?;
    write!(f, ")")
  }
}

#[derive(Debug, PartialEq, Clone)]
pub struct QExpression {
  pub expressions: VecDeque<LVal>,
}

impl fmt::Display for QExpression {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{{")?;
    write_expressions(f, &self.expressions)?;
    write!(f, "}}")
  }
}

#[derive(Debug, PartialEq, Clone)]
pub struct IntegerExpression {
  pub val: i64,
}

impl fmt::Display for IntegerExpression {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}", self.val)
  }
}

#[derive(Debug)]
pub struct LEnv<'a> {
  parent: Option<&'a LEnv<'a>>,
  pub env: HashMap<usize, LVal>,
  pub name: &'static str,
}

impl<'a> LEnv<'a> {
  pub fn get(&self, key: &Symbol) -> Result<LVal, EvaluationError> {
    match self.env.get(&key.id) {
      Some(v) => Ok(v.clone()),
      None => match self.parent {
        Some(p) => p.get(key),
        None => Err(EvaluationError::SymbolNotDefined(key.clone())),
      },
    }
  }

  pub fn with_builtins(add_builtins: fn(lenv: &mut LEnv)) -> LEnv<'static> {
    let mut env =  LEnv{
      parent: None,
      env: HashMap::new(),
      name: "root",
    };
    add_builtins(&mut env);
    env
  }

  pub fn child(&'a self, name: &'static str) -> LEnv<'a> {
    LEnv{
      parent: Some(self),
      env: HashMap::new(),
      name,
    }
  }

  pub fn define_builtin(&mut self, name: &str, func: LispFn) {
    let symbol = get_symbol(name);
    self
      .env
      .insert(symbol.id, LVal::Builtin(BuiltinFn { func }));
  }
}

type ExpectedType = &'static str;
type Caller = &'static str;

#[derive(PartialEq, Debug)]
pub struct UnexpectedExpression {
  expected: ExpectedType,
  callsite: &'static str,
  got: Caller,
}

#[derive(PartialEq, Debug)]
pub enum EvaluationError {
  DivideByZero,
  UnexpectedExpression(UnexpectedExpression),
  WantMoreExpressions(&'static str),
  SymbolNotDefined(Symbol),
}

impl fmt::Display for EvaluationError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    use self::EvaluationError::*;
    match self {
      DivideByZero => write!(f, "Error: Divide by zero"),
      UnexpectedExpression(ue) => write!(f, "in: {} wanted: {} got: {}",
      ue.callsite, ue.expected, ue.got),
      WantMoreExpressions(msg) => write!(f, "Expected more expression: {}", msg),
      SymbolNotDefined(s) => write!(f, "symbol: {} not defined", s),
    }
  }
}


pub fn unexpected_expression(
  expected: &'static str,
  callsite: &'static str,
  got: LVal,
) -> EvaluationError {
  EvaluationError::UnexpectedExpression(UnexpectedExpression {
    expected,
    callsite: callsite,
    got: got.name(),
  })
}

pub type LispFn = fn(lenv: &mut LEnv, lval: LVal) -> Result<LVal, EvaluationError>;

#[derive(Clone)]
pub struct BuiltinFn {
  pub func: &mut LispFn,
}

impl PartialEq for BuiltinFn {
  fn eq(&self, other: &Self) -> bool {
    self as *const _ == other as *const _
  }
}

impl fmt::Debug for BuiltinFn {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "<function>")
  }
}

impl fmt::Display for BuiltinFn {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "<function>")
  }
}

#[derive(Debug, PartialEq, Clone)]
pub struct UserFn<&'a> {
  pub env: &mut LEnv
  pub formals: QExpression,
  pub body: QExpression,
}

impl UserFn {
  fn apply(&self, arguments: VecDeque<LVal>) -> Result<LVal, EvaluationError> {
    
  }
}

impl fmt::Display for UserFn {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "(\\ {} {})", self.formals, self.body)
  }
}

#[derive(Debug, PartialEq, Clone)]
pub enum LVal {
  Integer(IntegerExpression),
  Symbol(Symbol),
  SExpression(SExpression),
  QExpression(QExpression),
  Builtin(BuiltinFn),
  UserFn(UserFn),
}

impl LVal {
  pub fn integer(self, caller: &'static str) -> Result<IntegerExpression, EvaluationError> {
    match self {
      LVal::Integer(ie) => Ok(ie),
      lval => Err(unexpected_expression("Integer", caller, lval)),
    }
  }
  pub fn symbol(self, caller: &'static str) -> Result<Symbol, EvaluationError> {
    match self {
      LVal::Symbol(s) => Ok(s),
      lval => Err(unexpected_expression("Symbol", caller, lval)),
    }
  }
  pub fn sexpr(self, caller: &'static str) -> Result<SExpression, EvaluationError> {
    match self {
      LVal::SExpression(sexpr) => Ok(sexpr),
      lval => Err(unexpected_expression("sexpr", caller, lval)),
    }
  }
  pub fn qexpr(self, caller: &'static str) -> Result<QExpression, EvaluationError> {
    match self {
      LVal::QExpression(qexpr) => Ok(qexpr),
      lval => Err(unexpected_expression("qexpr", caller, lval)),
    }
  }
  pub fn function(self, caller: &'static str) -> Result<LispFn, EvaluationError> {
    match self {
      LVal::Builtin(f) => Ok(f.func),
      lval => Err(unexpected_expression("function", caller, lval)),
    }
  }
  pub fn sub_expressions(self, caller: &'static str) -> Result<VecDeque<LVal>, EvaluationError> {
    match self {
      LVal::QExpression(qexpr) => Ok(qexpr.expressions),
      LVal::SExpression(sexpr) => Ok(sexpr.expressions),
      lval => Err(unexpected_expression("sexpr or qexpr", caller, lval)),
    }
  }

  pub fn name(&self) -> &'static str {
    use self::LVal::*;
    match self {
      Integer(_) => "Integer",
      Symbol(_) => "Symbol",
      SExpression(_) => "SExpression",
      QExpression(_) => "QExpression",
      Builtin(_) => "Builtin",
      UserFn(_) => "Function",
    }
  }
}

impl fmt::Display for LVal {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    use self::LVal::*;
    match self {
      Integer(ie) => write!(f, "{}", ie),
      // Operator(o) => write!(f, "{}", o),
      SExpression(se) => write!(f, "{}", se),
      QExpression(qe) => write!(f, "{}", qe),
      Symbol(s) => write!(f, "{}", s),
      Builtin(b) => write!(f, "{}", b),
      UserFn(u) => write!(f, "{}", u),
    }
  }
}

#[derive(Debug, Clone)]
pub struct Program {
  pub root: LVal,
}

impl fmt::Display for Program {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}", self.root)
  }
}

#[derive(Debug)]
pub struct ParseError {
  token: lexer::TokenValue,
  msg: &'static str,
}

struct Parser {
  tokens: VecDeque<lexer::TokenValue>,
  current_token: usize,
}

fn get_symbol(name: &str) -> Symbol {
  let mut locked = SYMBOL_TO_ID.write().unwrap();
  let id = locked.entry(name.to_owned()).or_insert_with(|| {
    let this_id = NEXT_ID.inc();
    ID_TO_SYMBOL
      .write()
      .unwrap()
      .insert(this_id, name.to_owned());
    this_id
  });
  Symbol { id: *id }
}

impl Parser {
  fn advance(&mut self) -> lexer::TokenValue {
    if !self.is_at_end() {
      self.current_token += 1;
    }
    self.previous()
  }

  fn peek(&self) -> lexer::TokenValue {
    self.tokens.get(self.current_token).unwrap().clone()
  }

  fn previous(&self) -> lexer::TokenValue {
    self.tokens.get(self.current_token - 1).unwrap().clone()
  }

  fn is_at_end(&self) -> bool {
    if self.current_token >= self.tokens.len() {
      return true;
    }
    if self.peek().value == lexer::Token::EOF {
      return true;
    }
    return false;
  }

  fn parse_integer_expression(&mut self) -> Result<IntegerExpression, ParseError> {
    let token = self.previous();
    let val = match token.value {
      lexer::Token::IntegerLiteral(val) => val,
      _ => {
        return Err(ParseError {
          msg: "expected integer expression",
          token: token,
        })
      }
    };
    Ok(IntegerExpression { val: val })
  }

  fn parse_expression(&mut self) -> Result<LVal, ParseError> {
    if self.is_at_end() {
      return Err(ParseError {
        msg: "expected start of expression, hit EOF",
        token: self.previous(),
      });
    }
    let token = self.advance();

    use lexer::Token::*;

    match token.value {
      IntegerLiteral(_) => Ok(LVal::Integer(self.parse_integer_expression()?)),
      LeftParen => Ok(LVal::SExpression(self.parse_s_expression()?)),
      LeftBrace => Ok(LVal::QExpression(self.parse_q_expression()?)),
      SymbolToken(s) => {
        let mut locked = SYMBOL_TO_ID.write().unwrap();
        let id = locked.entry(s.clone()).or_insert_with(|| {
          let this_id = NEXT_ID.inc();
          ID_TO_SYMBOL.write().unwrap().insert(this_id, s);
          this_id
        });
        Ok(LVal::Symbol(Symbol { id: *id }))
      }
      _ => Err(ParseError {
        msg: "expected left paren, left brace, integer, operator, or symbol",
        token: token,
      }),
    }
  }

  fn parse_s_expression(&mut self) -> Result<SExpression, ParseError> {
    use lexer::Token::*;
    // s expressions should start with a left paren
    let left_paren = self.previous();
    match &left_paren.value {
      lexer::Token::LeftParen => {}
      _ => {
        return Err(ParseError {
          msg: "expected left paren",
          token: left_paren,
        })
      }
    }
    let mut expressions = VecDeque::new();
    // parse the rest of the expression
    loop {
      let next = self.peek();
      // if we encounter a right paren, stop parsing this expression
      match next.value {
        RightParen => {
          self.advance();
          break;
        }
        EOF => {
          return Err(ParseError {
            msg: "expected expression or right paren, got EOF",
            token: next,
          })
        }
        _ => {}
      }
      expressions.push_back(self.parse_expression()?);
    }
    Ok(SExpression { expressions })
  }

  fn parse_q_expression(&mut self) -> Result<QExpression, ParseError> {
    use lexer::Token::*;
    // q expressions should start with a left brace
    let left_brace = self.previous();
    match left_brace.value {
      lexer::Token::LeftBrace => {}
      _ => {
        return Err(ParseError {
          msg: "expected left brace",
          token: left_brace,
        })
      }
    }
    let mut expressions = VecDeque::new();
    // parse the rest of the expression
    loop {
      let next = self.peek();
      // if we encounter a right paren, stop parsing this expression
      match next.value {
        RightBrace => {
          self.advance();
          break;
        }
        EOF => {
          return Err(ParseError {
            msg: "expected expression or right brace, got EOF",
            token: next,
          })
        }
        _ => {}
      }
      expressions.push_back(self.parse_expression()?);
    }
    Ok(QExpression { expressions })
  }

  fn parse_program(&mut self) -> Result<Program, ParseError> {
    let mut expressions = VecDeque::new();
    // parse the rest of the expression
    loop {
      let next = self.peek();
      // parse program until EOF
      match next.value {
        lexer::Token::EOF => break,
        _ => {}
      }
      expressions.push_back(self.parse_expression()?);
    }
    let root = if expressions.len() == 1 {
      expressions.pop_front().unwrap()
    } else {
      LVal::SExpression(SExpression {
        expressions: expressions,
      })
    };
    Ok(Program { root: root })
  }
}

pub fn parse(program: &str) -> Result<Program, ParseError> {
  // tokenize
  let tokens = match lexer::scan(program, "repl") {
    Err(e) => {
      eprintln!("{:?}", e);
      return Err(ParseError {
        msg: "unable to tokenize",
        token: lexer::TokenValue {
          end: lexer::Location {
            col: 0,
            line: 0,
            offset: 0,
          },
          start: lexer::Location {
            col: 0,
            line: 0,
            offset: 0,
          },
          text: "unable to tokenize".to_owned(),
          value: lexer::Token::EOF,
        },
      });
    }
    Ok(t) => t,
  };
  // parse
  let mut parser = Parser {
    tokens: VecDeque::from(tokens),
    current_token: 0,
  };
  parser.parse_program()
}
